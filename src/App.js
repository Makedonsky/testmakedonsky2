import React, { useEffect, useState } from "react";
import Body from "./components/Body/Body.js";
import Footer from "./components/Footer/Footer.js";
import Header from "./components/Header/Header";
import cl from "./App.css";
import axios from "axios";

const App = () => {
  const [repositories, setRepositories] = useState([]);
  const [question, setQuestion] = useState("stavropol");

  const getRepositories = () => {
    axios.get(
        `https://api.github.com/search/repositories?q=${question}`
      )
      .then((res) => setRepositories(res.data.items));
  };

  useEffect(() => {
    getRepositories();
  }, []);

  console.log(repositories);

  return (
    <div className={cl.wrap}>
      <Header setQuestion={setQuestion} getRepositories={getRepositories} />
      <Body repositories={repositories} />
      <Footer />
    </div>
  );
};

export default App;
