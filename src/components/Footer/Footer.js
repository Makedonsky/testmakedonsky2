import cl from "./Footer.module.css";
import Foot from "../../assets/Footer.png"

const Footer = () => {
  return (
    <div className={cl.wrap}>
      <img className = {cl.img} src={Foot}/>
    </div>
  );
};

export default Footer;
