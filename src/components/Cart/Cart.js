import cl from "./Cart.module.css";
import Pencil from "../../assets/Pencil.png";
import Star from "../../assets/Star.png";
import Eye from "../../assets/Eye.png"; 
const Cart = (props) => {
  return (
    <div className={cl.cart_wrap}>
      <p className={cl.projectname}>{props.unit.full_name}</p>
      <div style={{ display: "flex",  cursor: 'pointer' }} onClick = {() => window.open(props.unit.clone_url)}>
        <img className={cl.avatar} src={props.unit.owner.avatar_url}/>
        <p className={cl.author}>{props.unit.name}</p>
      </div>

      <div className={cl.groupone}>
        <img src={Star} width='15px' height='15px'/>
        <p className={cl.count_star}>{props.unit.stargazers_count}</p>
        <img src={Eye} width='15px' height='15px'/>
        <p>{props.unit.watchers_count}</p>
      </div>

     <div className={cl.groupthree}>
        <input className = {cl.input}></input>
        <img src={Pencil} width="25px" />
      </div>
    </div>
  );
};
export default Cart;
