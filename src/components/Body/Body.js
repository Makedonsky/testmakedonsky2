import Cart from "../Cart/Cart";
import cl from "./Body.module.css";
const Body = (props) => {
  return (
    <div className={cl.wrap}>
      {props.repositories.map((unit) => (
        <Cart unit={unit} />
      ))}
    </div>
  );
};

export default Body;
