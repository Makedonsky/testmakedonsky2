import cl from './Header.module.css'
import SearchButton from '../../assets/SearchButton.png'

const Header = (props) => {
    return(
        <div className={cl.wrap}>
           <div className={cl.rect1}>
           <input className={cl.pole} onChange={e => props.setQuestion(e.target.value)}/>
            <img src={SearchButton} onClick={props.getRepositories} className={cl.img}/>

           </div>
           
        </div>
    )
}

export default Header